package yulya.sanchak;

import java.io.*;
import java.util.*;

public class App {
    public static void main(String[] args) {
        List<String> paths = new ArrayList<>();
        for (String path : args) {
            paths.add(path);
        }

        Map<String, Set<String>> data = new HashMap<>();
        List<Thread> threadsReader = new ArrayList<>(paths.size());
        Thread t;
        for (String filepath : paths) {
            t = new Thread(new CsvFileReader(filepath, data));
            t.setName("Thread " + filepath);
            threadsReader.add(t);
            t.start();
        }
        for (Thread th : threadsReader) {
            try {
                th.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        List<Thread> threadsWriter = new ArrayList<>(data.keySet().size());
        for (String key : data.keySet()) {
            t = new Thread(new CsvFileWriter("data/" + key + ".csv", data.get(key)));
            t.setName("Thread " + key);
            threadsWriter.add(t);
            t.start();
        }
        for (Thread th : threadsWriter) {
            try {
                th.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
