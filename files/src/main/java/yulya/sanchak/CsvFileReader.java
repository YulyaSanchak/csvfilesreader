package yulya.sanchak;

import java.io.*;
import java.util.*;

public class CsvFileReader implements Runnable{
    private Map<String, Set<String>> raws;
    private String filepath;

    public CsvFileReader(String filepath, Map<String,Set<String>>raws) {
        this.filepath = filepath;
        this.raws = raws;
    }

    public void run (){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File(filepath)));

            String line = reader.readLine();
            String[] firstLine = line.split(";");
            for(String str: firstLine){
                if(!raws.containsKey(str)) {
                    raws.put(str, new HashSet<String>());
                }
            }
            String[] currentLine;
            while ((line= reader.readLine()) != null) {
                currentLine = line.split(";");
                for(int i=0;i<currentLine.length;i++){
                    raws.get(firstLine[i]).add(currentLine[i]);
                }
                System.out.printf("%s %s \n", Thread.currentThread().getName(), line);
                Thread.sleep(1000);
            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

    }


}
