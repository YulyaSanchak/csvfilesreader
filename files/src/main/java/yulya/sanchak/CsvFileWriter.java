package yulya.sanchak;

import java.io.*;
import java.util.Set;

public class CsvFileWriter implements Runnable {
    private String filepath;
    private Set<String> data;

    public CsvFileWriter(String filepath, Set<String> data) {
        this.filepath = filepath;
        this.data = data;
    }

    public void run(){
        try {
            BufferedWriter writer=new BufferedWriter(new FileWriter(new File(filepath)));
            for(String value: data){
                writer.write(value+";");
                System.out.printf("%s %s \n", Thread.currentThread().getName(), value);
                Thread.sleep(1000);
            }
            writer.flush();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

}
